import Vue from 'vue';
import VueRouter from 'vue-router';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import vuetify from './plugins/vuetify';
import Login from './components/Login.vue'
import Main from './components/Main.vue'
import '@/assets/app.css'

library.add(faUserSecret)
 
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false
Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    { path: '/', component: Login },
    { path: '/main', component: Main }
  ]
});
new Vue({
  router,
  vuetify,
  template: `
<div>
<nav style="height:30px;">
<div class="collapse navbar-collapse" id="navbarNav">
  <ul class="navbar-nav">
    <li class="nav-item"><router-link to="/" class="nav-link">Home</router-link></li>
    <li class="nav-item"><router-link to="/main" class="nav-link">Main</router-link></li>
  </ul>
</div>
</nav>
<div>
<router-view class="view"></router-view>
</div>
</div>
`
}).$mount('#app')